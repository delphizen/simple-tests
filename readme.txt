Delphi + TDD, Task #1

Open SimpleGroup - this Delphi project group contains 2 projects:
- SimpleApp - console application with product logic, almost empty for now
- SimpleAppTests - 2 DUnit test cases with instructions

Activate and run SimpleAppTests project. It will show list of tests. Press
F9 to run and you will see that they all fail. Your task is to fix this.

Start with uTest1.pas. After each modification, run your all test case. I
reccomend using Ctrl+F9 instead of F9, this is faster.

--
(c) 2013 contact@frantic.im

