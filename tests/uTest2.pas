(**
 * This is a part of Delphi+TDD course
 * (c) 2013 contact@frantic.im
 *)

unit uTest2;

interface

uses
  TestFramework, uPolishEval;

  // RED - GREEN - REFACTOR
  // These 3 words are TDD mantra. At first, you write test, that fails (red).
  // Then you make that test pass (green) by writing some code. Keep your
  // focus on making test pass, not making perfect code. On the last step
  // (refactor) you clean up your green code, making sure it doesn't fail.

  // Your task is to write Polish notation evaluator
  // http://en.wikipedia.org/wiki/Polish_notation
  // Tests are provided for you, they all fail. You need to write PolishEval
  // function using Red-Green-Refactor strategy. Take tests one-by-one.

  // Don't forget to commit your code after each step (instructions are given)

type
  TPolishCalculatorTestCase = class(TTestCase)
  published
    procedure TestEmptyInput;
    procedure TestSimpleNumber;
    procedure TestSimpleSum;
    procedure TestComplexOperations;
    procedure TestInvalidInput;
    procedure TestExtra;
  end;

implementation

{ TPolishCalculatorTestCase }

procedure TPolishCalculatorTestCase.TestEmptyInput;
begin
  // Note how the test defines our function interface. I don't use fancy
  // patterns, classes, interfaces, factories etc. I start from the simplest
  // usecase. Of course, you might want to make different decision on
  // how this function should look like, and you will have this opportunity in
  // next chapters
  CheckEquals(0, PolishEval([]), 'Empty input should eval to 0');

  // Commit your code now: $ git commit -am "Completed TestEmptyInput"
end;

procedure TPolishCalculatorTestCase.TestSimpleNumber;
begin
  // PolishEval takes array of variant elements and returns results of
  // calculations. If there is only one element and it is a number, just return
  // it
  CheckEquals(42, PolishEval([42]));

  // Commit your code now: $ git commit -am "Completed TestSimpleNumber"
end;

procedure TPolishCalculatorTestCase.TestSimpleSum;
begin
  // Now real example. Note how we pass operations, they are strings!
  CheckEquals(4, PolishEval(['+', 2, 2]));

  // Commit your code now: $ git commit -am "Completed TestSimpleSum"
end;

procedure TPolishCalculatorTestCase.TestComplexOperations;
begin
  CheckEquals(42, PolishEval(['+', 2, '*', 8, 5]));

  // Commit your code now: $ git commit -am "Completed TestComplexOperations"
end;

procedure TPolishCalculatorTestCase.TestInvalidInput;
begin
  // But what happens when input is invalid? = Bad question
  // What do we want to happen when input is invalid?
  // Lets imagine we want our code to raise specific exception
  ExpectedException := EBadInput;
  PolishEval(['*']);

  // Commit your code now: $ git commit -am "Completed TestInvalidInput"
end;


procedure TPolishCalculatorTestCase.TestExtra;
begin
  // Now its your turn. Imagine you want to add something to your Polish
  // calculator. For example, support of "max" operator, or "sin" function,
  // or ....

  // Write your test here. IMPORTANT: It should fail at the beginning (remember
  // red-green-refactor)

  // Commit your code now: $ git commit -am "Completed TestExtra"
end;


// DONE! Congratz!
// Now, push your code to BitBucket server:
//
// $ git push origin master
//
// Don't forget to add contact@frantic.im to your repository users. Email me
// if you have any difficulties doing this.


initialization
  RegisterTest(TPolishCalculatorTestCase.Suite);

end.
