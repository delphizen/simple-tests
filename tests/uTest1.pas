(**
 * This is a part of Delphi+TDD course
 * (c) 2013 contact@frantic.im
 *)

unit uTest1;

interface

uses
  TestFramework, SysUtils;

  // Your task is to fix following test cases by replacing placeholder __
  // with something different (Integer, string, etc.)

  // Read the comments carefuly
  // Re-run test suite after each change and watch results

type
  TBasicsTestCase = class(TTestCase)
  // Note, all tests are "published"
  published
    procedure TestTruth;
    procedure TestSimpleMath;
    procedure TestSimpleStrings;
    procedure TestUnexpectedException;
    procedure TestExpectedException;
    procedure TestEmptyness;
  end;

implementation

{ TBasicsTestCase }

// Is is just a placeholder. You will need to replace it with something
// in each test below
var __: Variant;

procedure TBasicsTestCase.TestTruth;
begin
  // Tests are all about checking things. This test will pass
  // only if its argument evaluates to True
  CheckTrue(__);
end;

procedure TBasicsTestCase.TestSimpleMath;
begin
  // In most cases, you want to test some process. One way to do that is 
  // to execute this process with pre-defined arguments and compare expected
  // and actual results
  Check(__ = (2 + 2));

  // But the example above is not very good. When this check fails, DUnit
  // marks this test as failed, but doesn't show any helpful information
  // CheckEquals is much better solution. Note the arguments order:
  // CheckEquals ( **expected result**, **actual result**, comments)
  CheckEquals(__, 2 + 2);

  // Ofcourse, to make output even better, you can supply additional
  // comments, like so:
  CheckEquals(__, 2 + 2, 'Compiler is crazy, it thinks 2 + 2 <> 4!');

  // In complex cases, always use additional comments. This will remind you
  // whats wrong when some old test fails
end;

procedure TBasicsTestCase.TestSimpleStrings;
begin
  // CheckEquals is overloaded method. It works with different types of
  // arguments: Booleans, Integers, Strings, Classes, etc.
  CheckEquals(__, 'hello-world');
end;

procedure TBasicsTestCase.TestUnexpectedException;
begin
  // Sometimes your code raises an exception. When this happens, DUnit marks
  // your test as FAILED. And this is awesome! You will never miss that again!
  raise Exception.Create('Unexpected internal error');

  // Exceptions might not be obvious at first, this is why we test our code
  CheckEquals(1, 1 / __);
end;

procedure TBasicsTestCase.TestExpectedException;
begin
  // But what if you EXPECT your code to raise an exception? DUnit has
  // way to check that too! Just start your test with following:
  ExpectedException := EDivByZero;

  // And now your test will fail if it doesn't raise provided exeption
  raise Exception.Create('Wrong exception');
end;

procedure TBasicsTestCase.TestEmptyness;
begin
  // Tests without code fail. Add any code here to make test green,
  // dummy YieldProcessor for example
end;

// Now, please commit your code. You can do that from Git Bash, just cd to
// your project dir and execute:
//
// $ git commit -am "Completed simple test case"
//

// Take a look at following lines - evey test case should end like this,
// so DUnit knows it needs to run it

// Now proceed to uTest2

initialization
  RegisterTest(TBasicsTestCase.Suite);

end.
