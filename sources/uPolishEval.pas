unit uPolishEval;

interface

uses
  SysUtils;

type
  ENotImplemented = class(Exception);
  EBadInput = class(Exception);

function PolishEval(Input: array of const): Extended;

implementation

function PolishEval(Input: array of const): Extended;
begin
  // Don't try to make this function complete and perfect from the beginning
  // Instead, take small steps according to the test case
  raise ENotImplemented.Create('You need to implement me. Step-by-step');
end;

end.
