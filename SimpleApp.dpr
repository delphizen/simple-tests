program SimpleApp;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  uPolishEval in 'sources\uPolishEval.pas';

begin
  try
    WriteLn('Hello, world!');
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
